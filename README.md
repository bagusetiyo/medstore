# Changelog MedStore

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.
		
# [3.2.0.28] - 2021-03-17
## Menu Import Data Obat
- **Fixed :**
	1. Fix error no. faktur import untuk obat konsinyasi dan non konsinyasi.

# [3.2.0.27] - 2021-02-08
## Menu Stok Opname
- **Fixed :**
	1. Fix error salah perhitungan obat konsinyasi ketika selisih minus.		

# [3.2.0.26] - 2020-07-23
## Menu Penjualan Obat
- **Added :**
	1. Tambah proteksi cek faktur ganda di 2 komputer berbeda.

# [3.2.0.25] - 2019-08-28
## Menu Retur Pembelian Obat
- **Fixed :**
	1. Fix error salah hitung perkalian total retur ketika menggunakn scroll mouse pada saat memilih item.
## Menu Retur Penjualan Obat
- **Fixed :**
	1. Fix error salah hitung perkalian total retur ketika menggunakn scroll mouse pada saat memilih item.

# [3.2.0.24] - 2018-11-16
## Menu Retur Pembelian Obat
- **Fixed :**
	1. Fix error `dataset "" does not exist` ketika ditekan tombol cetak.

# [3.2.10.0.23] - 2018-09-07
## Menu Pembenahan Data
- **Fixed :**
	1. Fix data junal keuangan tidak terhapus ketika pilihan "SEMUA DATA TRANSAKSI, DATA STOK DAN HARGA OBAT TETAP".
	2. Fix data junal keuangan tidak terhapus ketika pilihan "SEMUA DATA TRANSAKSI, DATA STOK DAN HARGA OBAT KOSONG".

# [3.2.10.0.22] - 2018-07-05
## Menu Data Obat
- **Added :**
	1. Pilihan sorting berdasarkan Kode Obat atau Nama Obat.

## Menu Pembelian Obat
- **Changed :**
	1. Optimasi menu.
- **Fixed :**
	1. Fix total pembelian tidak sesuai dengan perkalian jumlah beli dan harga beli.

## Menu Retur Pembelian Obat
- **Changed :**
	1. Tabel data obat yang diretur tambah kolom harga retur satuan.
- **Fixed :**
	1. Fix total retur tidak sesuai dengan perkalian jumlah retur dan harga retur.

## Menu Penjualan Obat
- **Changed :**
	1. Harga Jual tidak bisa diubah.
- **Fixed :**
	1. Fix jumlah total penjualan obat melebihi stok yang tersedia ketika diubah.

## Menu Retur Penjualan Obat
- **Changed :**
	1. Tabel data obat yang diretur tambah kolom harga retur satuan.
- **Fixed :**
	1. Fix total retur tidak sesuai dengan perkalian jumlah retur dan harga retur.

## Menu Laporan Obat Expired Dan Obat Habis
- **Added :**
	1. Pilihan sorting berdasarkan Kode Obat atau Nama Obat.
- **Changed :**
	1. Default pilihan export excel menggunakan "fast export" untuk memepercepat proses export data ke excel.
	2. Format angka sudah menggunakan auto thousand separator.
- **Fixed :**
	1. Tidak bisa mencetak laporan.

## Menu Pembayaran Piutang
- **Fixed :**
	1. Fixed Nama Pasien dan Dokter tidak tampil untuk Penjualan Resep.

## Menu Laporan Pembayaran Piutang
- **Fixed :**
	1. Fixed Nama Pasien dan Dokter tidak tampil untuk Penjualan Resep.

# [3.2.10.0.21] - 2018-02-09
## Menu Ganti Shift
- **Fixed :**
	1. Fix data shift yang bisa ditutup oleh user lain.

# [3.2.10.0.20] - 2017-12-03
## Menu Laporan Penjualan Obat
- **Fixed :**
	1. Fix perhitungan subtotal, diskon dan total di tabel.
	2. Fix perhitungan subtotal, diskon dan total di cetak laporan berdasarkan faktur.